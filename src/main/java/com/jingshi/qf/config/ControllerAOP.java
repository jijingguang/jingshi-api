package com.jingshi.qf.config;

import javax.security.auth.login.LoginException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.ResultBean;


/**
 * 处理和包装异常
 */
@Aspect
@Component
public class ControllerAOP {

	private static final Logger logger = LoggerFactory.getLogger(ControllerAOP.class);

	@Pointcut("execution(public * com.jingshi.qf.controller.*.*(..))")
	public void webLog() {
	}

	@Around("webLog()")
	public Object arround(ProceedingJoinPoint pjp) {
		long startTime = System.currentTimeMillis();

		ResultBean<?> result;

		try {
			result = (ResultBean<?>) pjp.proceed();
			logger.info(pjp.getSignature() + "use time:" + (System.currentTimeMillis() - startTime));
		} catch (Throwable e) {
			result = handlerException(pjp, e);
		}

		return result;
	}

	private ResultBean<?> handlerException(ProceedingJoinPoint pjp, Throwable e) {
		ResultBean<?> result = new ResultBean<Object>();

		if (e instanceof MyControlException) {
			result.setMsg(e.getMessage());
			result.setCode(ResultBean.FAIL);
		} else if (e instanceof LoginException) {
				result.setMsg(e.getMessage());
				result.setCode(ResultBean.NO_LOGIN);
		} else {
			logger.error(pjp.getSignature() + " error ", e);
			result.setMsg("系统异常，请联系管理员");
			result.setCode(ResultBean.FAIL);
		}

		return result;
	}
}
