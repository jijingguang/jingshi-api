package com.jingshi.qf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/***
* @Desciption: 拦截器
* @Date: 13:53 2018/1/15
* @return 
*/
//@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {
 
    public void addInterceptors(InterceptorRegistry registry) {
    	//addPathPatterns 用于添加拦截规则
    	//excludePathPatterns 用于排除拦截
        registry.addInterceptor(new TokenInterceptor()).addPathPatterns("/manager/**")
        		.excludePathPatterns("/**");
        
    }
    /**
     * 这个地方要重新注入一下资源文件，不然不会注入资源的，也没有注入requestHandlerMappping,相当于xml配置的
     *  <!--swagger资源配置-->
     *  <mvc:resources location="classpath:/META-INF/resources/" mapping="swagger-ui.html"/>
     *  <mvc:resources location="classpath:/META-INF/resources/webjars/" mapping="/webjars/**"/>
     * @param registry
     */
     @Override
     public void addResourceHandlers(ResourceHandlerRegistry registry) {
         registry.addResourceHandler("swagger-ui.html")
         .addResourceLocations("classpath:/META-INF/resources/");
         registry.addResourceHandler("/webjars/**")
         .addResourceLocations("classpath:/META-INF/resources/webjars/");
         registry.addResourceHandler("/csrf/**")
         .addResourceLocations("classpath:/META-INF/resources/csrf/");
         registry.addResourceHandler("/springfox-swagger-ui/**")
         .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/");
         registry.addResourceHandler("/images/**")
         .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/images/");
         registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
     }
	
}
