package com.jingshi.qf.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.SysUserModel;
import com.jingshi.qf.utils.TokenUtils;

public class TokenInterceptor implements HandlerInterceptor {
	 
	 
    private static Logger LOG = LoggerFactory.getLogger(TokenInterceptor.class);
 
    @Override
    public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
 
    	HttpServletRequest req = (HttpServletRequest) arg0;
//		HttpServletResponse res = (HttpServletResponse) arg1;
//		String tokenStr=req.getParameter("token");
		String tokenStr=req.getHeader("token");
        LOG.info("登录拦截的URL:{},登录拦截获取参数token字符串:{}",req.getRequestURI(),tokenStr);
        
        if(!StringUtils.isEmpty(tokenStr)){
        	SysUserModel sysUserModel=TokenUtils.getToken(tokenStr);
        	LOG.info("登录拦截解析token字符串获取token对象:{}",JSON.toJSONString(sysUserModel));
			if(sysUserModel!=null){
				//TODO验证用户有效性
				return true;
			}
		}
		PrintWriter writer = null;
		arg1.setCharacterEncoding("UTF-8");
		arg1.setContentType("text/html; charset=utf-8");
		try {
			writer = arg1.getWriter();
			ResultBean<Object> resultBean = new ResultBean<>();
			resultBean.setSuccess(true);
			resultBean.setMsg("请先登录！");
//                String error = "token信息有误";
			writer.print(resultBean);
			return false;

		} catch (IOException e) {
			LOG.error("response error", e);
		} finally {
			if (writer != null)
				writer.close();
		}
		return true;
    }
 
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
 
 
    }
 
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
 
    }
}
