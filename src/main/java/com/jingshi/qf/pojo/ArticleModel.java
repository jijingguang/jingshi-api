package com.jingshi.qf.pojo;

public class ArticleModel {
	
	private Long id;
	
	private String articleTitle;
	
	private String labTitle;
	
	private String content;
	
	private Integer categoryParent; //上级分类
	
	private Integer categoryId;
	
	private String categoryName;
	
	private String articleDescribe;
	
	private Integer lableId;
	
	private String keywords;
	
	private String imageUrl;
	
	private Long clickNum; //浏览量
	
	private String author; //作者
	
	private Integer isOpen; 
	
	private Integer isDelete;
	
	private Integer created; //创建人id
	
	private Integer modified; //最近修改人id

	private String createTime;

	private String updateTime;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public String getLabTitle() {
		return labTitle;
	}

	public void setLabTitle(String labTitle) {
		this.labTitle = labTitle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCategoryParent() {
		return categoryParent;
	}

	public void setCategoryParent(Integer categoryParent) {
		this.categoryParent = categoryParent;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getArticleDescribe() {
		return articleDescribe;
	}

	public void setArticleDescribe(String articleDescribe) {
		this.articleDescribe = articleDescribe;
	}

	public Integer getLableId() {
		return lableId;
	}

	public void setLableId(Integer lableId) {
		this.lableId = lableId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getClickNum() {
		return clickNum;
	}

	public void setClickNum(Long clickNum) {
		this.clickNum = clickNum;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getCreated() {
		return created;
	}

	public void setCreated(Integer created) {
		this.created = created;
	}

	public Integer getModified() {
		return modified;
	}

	public void setModified(Integer modified) {
		this.modified = modified;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
