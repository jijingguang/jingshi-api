package com.jingshi.qf.pojo.requestParam;

import java.util.List;

public class Param {
	
	private Integer pageNo = 1;

	private Integer pageSize = 10;
	
	private Integer categoryId;
	
	private String queryKeyword;
	
	private List<Integer> ids;

	
	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getQueryKeyword() {
		return queryKeyword;
	}

	public void setQueryKeyword(String queryKeyword) {
		this.queryKeyword = queryKeyword == null ? null : queryKeyword.trim() ;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}
	
	

}
