package com.jingshi.qf.pojo;

import java.io.Serializable;

public class RemoveConsultModel implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;
	
	private String mobile;
	
	private Integer removeScope; //动迁范围 0 其他 1 棚户区改造 2 宅基地 3 楼房 4 环保拆迁 5养殖场 6商铺 7违建拆除 8厂房 9企业
	
	private Integer removeSchedule; //动迁进度 0 未拆 1 已拆
	
	private Integer propertyFormalities; //房产手续 0 有 1 无
	
	private String url;
	
	private String createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getRemoveScope() {
		return removeScope;
	}

	public void setRemoveScope(Integer removeScope) {
		this.removeScope = removeScope;
	}

	public Integer getRemoveSchedule() {
		return removeSchedule;
	}

	public void setRemoveSchedule(Integer removeSchedule) {
		this.removeSchedule = removeSchedule;
	}

	public Integer getPropertyFormalities() {
		return propertyFormalities;
	}

	public void setPropertyFormalities(Integer propertyFormalities) {
		this.propertyFormalities = propertyFormalities;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	


}
