package com.jingshi.qf.pojo;

public class ResumeModel {
	
	private Long id;
	
	private String name;
	
	private String englishName;
	
	private String appellation;
	
	private String specialty;
	
	private String mainCase;
	
	private String societyJob;
	
	private String certificate;
	
	private String company;
	
	private Integer resumeOrder;
	
	private String imageUrl;
	
	private String bigImageUrl;
	
	private String smImageUrl;
	
	private Integer isDelete;
	
	private String createTime;
	
	private String updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getAppellation() {
		return appellation;
	}

	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public String getMainCase() {
		return mainCase;
	}

	public void setMainCase(String mainCase) {
		this.mainCase = mainCase;
	}

	public String getSocietyJob() {
		return societyJob;
	}

	public void setSocietyJob(String societyJob) {
		this.societyJob = societyJob;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public Integer getResumeOrder() {
		return resumeOrder;
	}

	public void setResumeOrder(Integer resumeOrder) {
		this.resumeOrder = resumeOrder;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getBigImageUrl() {
		return bigImageUrl;
	}

	public void setBigImageUrl(String bigImageUrl) {
		this.bigImageUrl = bigImageUrl;
	}

	public String getSmImageUrl() {
		return smImageUrl;
	}

	public void setSmImageUrl(String smImageUrl) {
		this.smImageUrl = smImageUrl;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	

}
