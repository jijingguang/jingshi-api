package com.jingshi.qf.pojo;


public class SysUserModel {
	
    private Integer userId;

    private String userNo;

    private String userName;

    private String pass;

    private String areaNo;

    private String phone;

    private String mail;

    private Byte isAdmin;

    private Byte status;

    private String createDate;

    private String lastLoginDate;

    private String lastLoginIp;

    private Integer olduserid;

    private Integer trainCompanyId;

    private Byte userType;


    public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo == null ? null : userNo.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass == null ? null : pass.trim();
    }

    public String getAreaNo() {
        return areaNo;
    }

    public void setAreaNo(String areaNo) {
        this.areaNo = areaNo == null ? null : areaNo.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public Byte getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp == null ? null : lastLoginIp.trim();
    }

    public Integer getOlduserid() {
        return olduserid;
    }

    public void setOlduserid(Integer olduserid) {
        this.olduserid = olduserid;
    }

    public Integer getTrainCompanyId() {
        return trainCompanyId;
    }

    public void setTrainCompanyId(Integer trainCompanyId) {
        this.trainCompanyId = trainCompanyId;
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

	@Override
	public String toString() {
		return "SysUserModel [userId=" + userId + ", userNo=" + userNo + ", userName=" + userName + ", pass=" + pass
				+ ", areaNo=" + areaNo + ", phone=" + phone + ", mail=" + mail + ", isAdmin=" + isAdmin + ", status="
				+ status + ", createDate=" + createDate + ", lastLoginDate=" + lastLoginDate + ", lastLoginIp="
				+ lastLoginIp + ", olduserid=" + olduserid + ", trainCompanyId=" + trainCompanyId + ", userType="
				+ userType + "]";
	}
    
    
}