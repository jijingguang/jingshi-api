package com.jingshi.qf;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.jingshi.qf.mapper")
public class JingshiApiApplication {
	
	private static Logger logger = LoggerFactory.getLogger(JingshiApiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(JingshiApiApplication.class, args);
		
		logger.info("SpringBoot Start Success");
	}

}
