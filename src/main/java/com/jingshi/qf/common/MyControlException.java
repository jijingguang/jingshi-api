package com.jingshi.qf.common;

public class MyControlException extends Exception {

	private static final long serialVersionUID = 1L;

	public MyControlException() {
		super();
	}

	public MyControlException (String message) {
        super(message);
    }

	public MyControlException (Throwable cause) {
        super(cause);
    }

	public MyControlException (String message, Throwable cause) {
        super(message, cause);
    }
}
