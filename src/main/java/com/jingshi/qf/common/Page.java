package com.jingshi.qf.common;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {

	private static final long serialVersionUID = 1L;;

	public static final int BASE_PAGE_NO = 1;
	
	public static final int BASE_PAGE_SIZE = 10;
	
	private int pageNo;

	private int pageSize;

	private List<T> list;

	private int count;

	public Page() {
		super();
	}
	
	public Page(List<T> list) {
		super();
		this.list = list;
	}
	public Page(List<T> list,int pageNo,int pageSize,int count){
		super();
		this.list = list;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.count = count;
	}
	
	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
