package com.jingshi.qf.common;

import java.io.Serializable;
import com.alibaba.fastjson.JSONObject;
import com.jingshi.qf.enums.ErrorCodes;

public class ResultBean<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;;
	
	public static final int NO_LOGIN = -1;
	
	public static final int SUCCESS = 0;
	
	public static final int FAIL = 1;
	
	public static final int NO_PERMISSION = 2;
	
	private String msg ;
	
	private int code;
	
	private boolean success;
	
	private Long total;
	
	private T data;
	
	public ResultBean() {
		super();
	}
	

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public ResultBean(T data) {
		super();
		this.data = data;
	}
	
	public ResultBean(Throwable e) {
		super();
		this.msg = e.toString();
		this.code = FAIL;
	}
	public ResultBean(boolean success, Integer code, String msg, T data,Long total) {
		super();
		this.success = success;
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.total = total;
	}
	
	public ResultBean(boolean success, Integer code, String msg, T data) {
		super();
		this.success = success;
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public ResultBean(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
	
	public ResultBean(boolean success, Integer code, String msg) {
		super();
		this.success = success;
		this.code = code;
		this.msg = msg;
	}
	
	public void response(boolean success,Integer code,String msg,T data){	
		this.success = success;
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	/**
     * 统一封装 无效参数
     * @param msg 消息
     */
	/*
	 * public static ResultBean<Object> invalidParam(String msg) {
	 * ResultBean<Object> reply = new ResultBean<>();
	 * reply.setCode(ErrorCodes.NOT_LOGIN.getCode()); reply.setSuccess(false);
	 * reply.setMsg(msg); return reply; }
	 */
	@Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}
}
