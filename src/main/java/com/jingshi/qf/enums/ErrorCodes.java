package com.jingshi.qf.enums;

public enum ErrorCodes {
	
	INVALID_PARAM(400,"求参数有误"),
	
	SERVER_ERR(500,"服务器异常"),
	
	SUCCESS(200,"请求成功"),
	
	NOT_LOGIN(10000, "未登录");
	
	private int code;// 错误代码
	private String msg;// 错误信息

	ErrorCodes(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}
}
