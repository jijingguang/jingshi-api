package com.jingshi.qf.mapper;

import java.util.List;
import java.util.Map;

import com.jingshi.qf.pojo.ArticleModel;

public interface ArticleMapper {
	
	int addArticle(ArticleModel articleModel)throws Exception;
	
	List<ArticleModel> queryArticleList(Map<String, Object> map)throws Exception;
	
	int queryCountArticleList(Map<String, Object> map)throws Exception;
	
	List<ArticleModel> queryArticleByCategoryId(Map<String, Object> map)throws Exception;
	
	int queryCountArticleByCategoryId(Map<String, Object> map)throws Exception;
	
	int updateArticle(ArticleModel articleModel)throws Exception;
	
	Integer deleteArticle(List<Integer> ids)throws Exception;
	
	ArticleModel queryArticleById(Long id)throws Exception;
	
	List<ArticleModel> queryArticleUpDown(Long id)throws Exception;
	
	List<ArticleModel> queryHotArticle()throws Exception;

}
