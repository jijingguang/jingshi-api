package com.jingshi.qf.mapper;

import java.util.List;
import java.util.Map;

import com.jingshi.qf.pojo.FileModel;

public interface VideoMapper {
	
	List<FileModel> queryVideoList(Map<String, Object> map)throws Exception;
	
	int queryCountVideoList(Map<String, Object> map)throws Exception;
	
	FileModel queryVideoById(Long id)throws Exception;

}
