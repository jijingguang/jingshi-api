package com.jingshi.qf.mapper;

import com.jingshi.qf.pojo.ConsultModel;
import com.jingshi.qf.pojo.RemoveConsultModel;

public interface ConsultMapper {
	
	int addConsult(ConsultModel consultModel)throws Exception;
	
	int addRemoveConsult(RemoveConsultModel removeConsultModel)throws Exception;

}
