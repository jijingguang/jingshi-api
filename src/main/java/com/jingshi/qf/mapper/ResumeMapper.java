package com.jingshi.qf.mapper;

import java.util.List;
import java.util.Map;

import com.jingshi.qf.pojo.ResumeModel;

public interface ResumeMapper {
	
	List<ResumeModel> queryResumeList(Map<String, Object> map)throws Exception;
	
	int queryCountResumeList(Map<String, Object> map)throws Exception;
	
	ResumeModel queryResumeById(Long id)throws Exception;

}
