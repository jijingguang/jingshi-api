package com.jingshi.qf.mapper;

import java.util.List;

import com.jingshi.qf.pojo.ArticleCategoryModel;

public interface ArticleCategoryMapper {
	
	int addArticleCategory(ArticleCategoryModel articleCategory)throws Exception;
	
	List<ArticleCategoryModel> queryArticleCategoryList()throws Exception;
	
	int queryCountArticleCategoryList()throws Exception;
	
	int updateArticleCategory(ArticleCategoryModel articleCategory)throws Exception;
	
	Integer deleteArticleCategory(List<Integer> ids)throws Exception;
	
	Integer queryCategoryParentById(Integer id)throws Exception;

}
