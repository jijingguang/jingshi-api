package com.jingshi.qf.utils;

import javax.security.auth.login.LoginException;

import org.springframework.util.StringUtils;

import com.jingshi.qf.pojo.SysUserModel;



public class TokenUtils {
	
	private static String key = "JSQF";
	
	/**
	 * 生成token字符串
	 * @param token
	 * @return
	 */
	public static String getTokenStr(SysUserModel token){
		String json=JsonUtil.obj2Json(token);
		return CipherUtil.encode(json, key);
	}
	
	/**
	 * 字符串转token
	 * @param tokenStr
	 * @return
	 */
	public static SysUserModel getToken(String tokenStr){
		//解密json
		if(!StringUtils.isEmpty(tokenStr)){
			String json=CipherUtil.decode(tokenStr, key);
			SysUserModel sysUserModel=JsonUtil.json2Obj(json, SysUserModel.class);
			return sysUserModel;
		}
		return null;
	}

	public static int getUserId(String tokenStr) throws Exception {
		try {

			if(!StringUtils.isEmpty(tokenStr)){
				String json=CipherUtil.decode(tokenStr, key);
				SysUserModel sysUserModel=JsonUtil.json2Obj(json, SysUserModel.class);
				
				int userId = sysUserModel.getUserId();
				if (userId <= 0) {
					throw new LoginException();
				}
				return userId;
			}else {
				throw new LoginException();
			}
		} catch (Exception e) {
			throw new LoginException("登录失败");
		}
	}
	/*
	 * public static Long getCompanyId(String tokenStr) throws Exception { try {
	 * 
	 * if(!StringUtils.isEmpty(tokenStr)){ String json=CipherUtil.decode(tokenStr,
	 * key); SysUserModel sysUserModel=JsonUtil.json2Obj(json, SysUserModel.class);
	 * 
	 * Integer companyId = sysUserModel.getTrainCompanyId(); if (companyId == null
	 * || companyId < 0) { throw new LoginException(); } return
	 * companyId.longValue(); }else { throw new LoginException(); } } catch
	 * (Exception e) { throw new LoginException("登录失败"); } }
	 */
	
	
//	public static void main(String[] aa){
//		String a="8065C2997DB1CF8B538080AA7C9CA37FF1FF781F4378695AD10CFC638C1190CD0DEE7BEB30282867E3947428528A3CDB87C2DAC15D261F36B54665C401DEDC264A276E63A515C28534CEA5218ADE4C9F83EB4AD43AB8F81E97E3E24BEF8145AEECD9B698C6F1A65EAAA73FDF32A666335B52DE56CC1F86949109E3968552A5FC";
//		Token token=getToken(a);
//		System.out.println(token.getPushId());
//	}
	
	

}
