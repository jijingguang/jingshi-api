package com.jingshi.qf.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class JsonUtil {
    private static Logger logger = LoggerFactory.getLogger(JsonUtil.class);

    private static ObjectMapper mapper = new ObjectMapper();

    public static <T> T json2Obj(String json, Class<T> type) {
        try {
            return mapper.readValue(json, type);
        } catch (Exception e) {
            logger.error("数据格式错误", e);
        }
        return null;
    }

    public static String obj2Json(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            logger.error("数据格式错误", e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> obj2Map(Object obj) {
        try {
            return mapper.convertValue(obj, HashMap.class);
        } catch (Exception e) {
            logger.error("数据格式错误", e);
        }
        return null;
    }

    /**
     * f返回json格式。
     *
     * @param <T>
     * @param response
     * @param data
     * @param isSuccess
     */
    public static <T> Object strToJsonObject(String data, T t) {
        try {
            if (StringUtils.equals(data, "error")) {
                return null;
            }
            ObjectMapper mapper = new ObjectMapper();
            T object = mapper.readValue(data, new TypeReference<T>() {
            });
            return object;
        } catch (Exception ex) {
            logger.error("数据格式错误", ex);
        }
        return null;
    }



}
