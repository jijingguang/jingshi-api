package com.jingshi.qf.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.alibaba.fastjson.JSON;

public class DateUtils {

	public static String DEFAULT_PATTERN = "yyyy-MM-dd";
	public static String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 *            日期
	 * @param format
	 *            日期格式
	 * @return 指定格式的日期字符串
	 */
	public static String formatDateByFormat(Date date, String format) {
		String result = "";
		if (date != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				result = sdf.format(date);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 转换为完整格式(yyyy-MM-dd HH:mm:ss)的日期字符串
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static String formatTimestampDate(Date date) {
		return formatDateByFormat(date, TIMESTAMP_PATTERN);
	}
	
	/**
	 * 转换为指定格式的日期字符串
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static String formatTimestampDate(Date date,String pattern) {
		return formatDateByFormat(date, pattern);
	}

	/**
	 * 转换为(yyyy-MM-dd)的日期字符串
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static String formatDatestampDate(Date date) {
		return formatDateByFormat(date, DEFAULT_PATTERN);
	}

	/**
	 * 字符串转换为默认格式(yyyy-MM-dd)日期对象
	 * 
	 * @param date
	 * 
	 * @return
	 * 
	 * @throws Exception
	 */
	public static Date parseDefaultDate(String date) {
		return parseDate(date, DEFAULT_PATTERN);
	}

	/**
	 * 日期格式字符串转换为日期对象
	 * 
	 * @param strDate
	 *            日期格式字符串
	 * @param pattern
	 *            日期对象
	 * @return
	 */
	public static Date parseDate(String strDate, String pattern) {
		try {
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			Date nowDate = format.parse(strDate);
			return nowDate;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取今天的日期 2017-8-23
	 * 
	 * @return
	 */
	public static Date getToday() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String format = sdf.format(new Date());
			return sdf.parse(format);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static Date getTomorrowDate() {
		try {
			Date date = new Date();// 取时间
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, 1);// 把日期往后加一天，若想把日期向前推一天则将正数改为负数
			date = calendar.getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = formatter.format(date);
			return formatter.parse(dateString);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("获取明天日期异常!");
		}
	}

	/**
	 * 从开始时间生成指定天数的日期
	 * 
	 * @param dBegin
	 *            开始日期
	 * @param days
	 *            天数
	 * @return
	 */
	public static List<Date> findDates(Date dBegin, int days) {
		List<Date> lDate = new ArrayList<Date>();
		// lDate.add(dBegin);
		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calBegin.setTime(dBegin);
		for (int i = 1; i <= days; i++) {
			lDate.add(calBegin.getTime());
			calBegin.add(Calendar.DAY_OF_MONTH, 1);
		}
		return lDate;
	}

	/**
	 * 给定一个指定日期 加上天数 得到加上天数的日期
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date getAddDaysDate(Date date, int days) {
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.DAY_OF_MONTH, days);
		return cl.getTime();
	}

	/**
	 * 判断两个日期是否相等
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		if ((date1 == null) || (date2 == null)) {
			throw new IllegalArgumentException("The date must not be null");
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return isSameDay(cal1, cal2);
	}

	public static boolean isSameDay(Calendar cal1, Calendar cal2) {
		if ((cal1 == null) || (cal2 == null)) {
			throw new IllegalArgumentException("The date must not be null");
		}
		return (cal1.get(0) == cal2.get(0)) && (cal1.get(1) == cal2.get(1))
				&& (cal1.get(6) == cal2.get(6));
	}
	
	/**
	 * 比较两个日期相差的天数
	 * @param fDate
	 * @param oDate
	 * @return
	 */
	public static int daysOfTwo(Date fDate, Date oDate) {
		
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.setTime(fDate);
		int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
		aCalendar.setTime(oDate);
		int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);
		if (fDate.after(oDate)) {
			return day1 - day2;
		}
		return day2 - day1;

	}
	
	/**
	 *  获取前后日期, i为正数 向后推迟i天，负数时向前提前i天
	 * @param i
	 * @return
	 */
	public static Date getDate(int i) {
		Calendar cd = Calendar.getInstance();
		cd.add(Calendar.DATE, i);
		Date time = cd.getTime();
		SimpleDateFormat dformat = new SimpleDateFormat(DEFAULT_PATTERN);
		String format = dformat.format(time);
		Date parseDate = parseDate(format, DEFAULT_PATTERN);
		return parseDate;
	}
	
	
	
	public static String formatToString(Object object) {
		return JSON.toJSONString(object);
	}
	
	
	 public static final String getDateStr()
	 {
	   Calendar cld = Calendar.getInstance();
	   
	   long bjTimeRowOff = TimeZone.getTimeZone("GMT+8").getRawOffset();
	   long defaultTimeRowOff = TimeZone.getDefault().getRawOffset();
	   if (bjTimeRowOff != defaultTimeRowOff) {
	     cld.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	   }
	   

	   String year = Integer.toString(cld.get(1));
	   String month = Integer.toString(cld.get(2) + 1);
	   if (month.length() == 1) {
	     month = '0' + month;
	   }
	   String day = Integer.toString(cld.get(5));
	   if (day.length() == 1) {
	     day = '0' + day;
	   }
	   return year + month + day;
	 }
	 /**
	  * 判断当天日期是否是月末
	  * @author syc
	  * @param date
	  * @return
	  */
	 public static boolean isMonthEnd(Date date){
		 Calendar calendar=Calendar.getInstance();
		 calendar.setTime(date);
		 if(calendar.get(Calendar.DATE)==calendar.getActualMaximum(Calendar.DAY_OF_MONTH)){
			 return true;
		 }else{
	         return false;
		 }
	 }
	 /**
	  * 获取指定月的第一天
	  * @Description: TODO
	  * @param @param date
	  * @param @return
	  * @param @throws Exception   
	  * @return String  
	  * @throws
	  * @author zsy
	  * @date 2018年2月27日
	  */
	 public static Date getMonthFirstDay(Date date){
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal_1 = Calendar.getInstance();
		cal_1.setTime(date);
		cal_1.set(Calendar.DAY_OF_MONTH,1);
		return cal_1.getTime();
	 }
	 /**
	  * 获取指定月的最后一天
	  * @Description: TODO
	  * @param @param date
	  * @param @return   
	  * @return String  
	  * @throws
	  * @author zsy
	  * @date 2018年2月27日
	  */
	 public static Date getMonthLastDay(Date date){
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal_1 = Calendar.getInstance();
		cal_1.setTime(date);
		cal_1.add(Calendar.MONTH, 1);
		cal_1.set(Calendar.DAY_OF_MONTH,0);
		return cal_1.getTime();
	 }
	 
	 public static Date formatDateToDateByFormat(Date date,String pattern) {
		 String strDate = formatDateByFormat(date, pattern);
		 Date parseDate = parseDate(strDate, pattern);
		 return parseDate;
		
	}

}
