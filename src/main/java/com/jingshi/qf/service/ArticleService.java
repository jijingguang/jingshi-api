package com.jingshi.qf.service;

import java.util.List;

import com.jingshi.qf.common.Page;
import com.jingshi.qf.pojo.ArticleModel;
import com.jingshi.qf.pojo.requestParam.Param;

public interface ArticleService {
	
	int addArticle(ArticleModel articleModel)throws Exception;
	
	Page<ArticleModel> queryArticleList(Param param)throws Exception;
	
	Page<ArticleModel> queryArticleByCategoryId(Param param)throws Exception;
	
	int updateArticle(ArticleModel articleModel)throws Exception;
	
	Integer deleteArticle(List<Integer> ids)throws Exception;
	
	ArticleModel queryArticleById(Long id)throws Exception;
	
	List<ArticleModel> queryArticleUpDown(Long id)throws Exception;
	
	List<ArticleModel> articleUpDown(Long id)throws Exception;
	
	List<ArticleModel> queryHotArticle()throws Exception;

}
