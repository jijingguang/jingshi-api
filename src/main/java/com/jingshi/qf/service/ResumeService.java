package com.jingshi.qf.service;

import com.jingshi.qf.common.Page;
import com.jingshi.qf.pojo.ResumeModel;
import com.jingshi.qf.pojo.requestParam.Param;

public interface ResumeService {
	
	Page<ResumeModel> queryResumeList(Param param)throws Exception;
	
	ResumeModel queryResumeById(Long id)throws Exception;

}
