package com.jingshi.qf.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.mapper.ConsultMapper;
import com.jingshi.qf.pojo.ConsultModel;
import com.jingshi.qf.pojo.RemoveConsultModel;
import com.jingshi.qf.service.ConsultService;
import com.jingshi.qf.utils.DateUtils;


@Service
public class ConsultServiceImpl implements ConsultService {
	
	@Autowired
	private ConsultMapper consultMapper;

	@Override
	public int addConsult(ConsultModel consultModel) throws Exception {
		
		consultModel.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		
		int i = consultMapper.addConsult(consultModel);
		
		if(i<=0) {
			throw new MyControlException("系统错误！添加失败！");
		}
		
		return i;
	}

	@Override
	public int addRemoveConsult(RemoveConsultModel removeConsultModel) throws Exception {
		
		removeConsultModel.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		int i = consultMapper.addRemoveConsult(removeConsultModel);
		
		if(i<=0) {
			throw new MyControlException("系统错误！添加失败！");
		}
		
		return i;
	}

}
