package com.jingshi.qf.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jingshi.qf.common.Page;
import com.jingshi.qf.mapper.VideoMapper;
import com.jingshi.qf.pojo.FileModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.VideoService;

@Service
public class VideoServiceImpl implements VideoService {
	
	@Value("${wangzhan.post}")
	private String post;
	
	@Autowired
	private VideoMapper videoMapper;

	@Override
	public Page<FileModel> queryVideoList(Param param) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", (param.getPageNo()-1)*param.getPageSize());
		map.put("end", param.getPageSize());
		
		Page<FileModel> page =new Page<FileModel>();
		
		List<FileModel> list = videoMapper.queryVideoList(map);
		for (FileModel fileModel : list) {
			if(StringUtils.isNoneEmpty(fileModel.getUrl())) {
				fileModel.setUrl(post+fileModel.getUrl());
			}
			if(StringUtils.isNoneEmpty(fileModel.getImgUrl())) {
				fileModel.setImgUrl(post+fileModel.getImgUrl());
			}
		}
		
		int count = videoMapper.queryCountVideoList(map);
		
		page.setList(list);
		page.setCount(count);
		page.setPageNo(param.getPageNo());
		page.setPageSize(param.getPageSize());
		
		return page;
	}

	@Override
	public FileModel queryVideoById(Long id) throws Exception {
		
		FileModel video = videoMapper.queryVideoById(id);
		
		if(StringUtils.isNoneEmpty(video.getUrl())) {
			video.setUrl(post+video.getUrl());
		}
		if(StringUtils.isNoneEmpty(video.getImgUrl())) {
			video.setImgUrl(post+video.getImgUrl());
		}
		
		return video;
	}

}
