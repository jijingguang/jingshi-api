package com.jingshi.qf.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.Page;
import com.jingshi.qf.mapper.ArticleCategoryMapper;
import com.jingshi.qf.pojo.ArticleCategoryModel;
import com.jingshi.qf.service.ArticleCategoryService;
import com.jingshi.qf.utils.DateUtils;

@Service
public class ArticleCategoryServiceImpl implements ArticleCategoryService {
	
	@Autowired
	private ArticleCategoryMapper articleCategoryMapper;
	
	@Override
	public int addArticleCategory(ArticleCategoryModel articleCategory) throws Exception {
		
		articleCategory.setIsDelete(0);
		articleCategory.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		articleCategory.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int i = articleCategoryMapper.addArticleCategory(articleCategory);
		if(i<=0) {
			throw new MyControlException("系统错误！添加失败！");
		}
		
		return i;
	}

	@Override
	public List<ArticleCategoryModel> queryArticleCategoryList() throws Exception {
		
		
		//查询数据
		List<ArticleCategoryModel> list = articleCategoryMapper.queryArticleCategoryList();
		
		
		
		return list;
	}

	@Override
	public int updateArticleCategory(ArticleCategoryModel articleCategory) throws Exception {
		
		articleCategory.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int i = articleCategoryMapper.updateArticleCategory(articleCategory);
		
		if(i<=0) {
			throw new MyControlException("系统错误！修改失败！");
		}
		
		return i;
	}

	@Override
	public Integer deleteArticleCategory(List<Integer> ids) throws Exception {
		
		return articleCategoryMapper.deleteArticleCategory(ids);
	}

}
