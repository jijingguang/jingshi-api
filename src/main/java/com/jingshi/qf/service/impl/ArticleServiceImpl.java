package com.jingshi.qf.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.Page;
import com.jingshi.qf.mapper.ArticleCategoryMapper;
import com.jingshi.qf.mapper.ArticleMapper;
import com.jingshi.qf.pojo.ArticleModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.ArticleService;
import com.jingshi.qf.utils.DateUtils;

@Service
public class ArticleServiceImpl implements ArticleService {
	
	@Value("${wangzhan.post}")
	private String post;
	
	@Autowired
	private ArticleMapper articleMapper;
	
	@Autowired
	private ArticleCategoryMapper articleCategoryMapper;

	@Override
	public int addArticle(ArticleModel articleModel) throws Exception {
		
		articleModel.setIsOpen(0);
		articleModel.setIsDelete(0);
		articleModel.setCreated(1);
		articleModel.setModified(1);
		articleModel.setCreateTime(DateUtils.formatTimestampDate(new Date()));
		articleModel.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int i = articleMapper.addArticle(articleModel);
		
		if(i<=0) {
			throw new MyControlException("系统错误！添加失败！");
		}
		
		return i;
	}

	@Override
	public Page<ArticleModel> queryArticleList(Param param) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("categoryId", param.getCategoryId());
		map.put("queryKeyword", param.getQueryKeyword());
		map.put("start", (param.getPageNo()-1)*param.getPageSize());
		map.put("end", param.getPageSize());
		
		Page<ArticleModel> page =new Page<ArticleModel>();
		
		List<ArticleModel> list = articleMapper.queryArticleList(map);
		for (ArticleModel articleModel : list) {
			//根据分类id查询父级分类
			Integer parentId = articleCategoryMapper.queryCategoryParentById(articleModel.getCategoryId());
			articleModel.setCategoryParent(parentId);
			
			if(StringUtils.isNoneEmpty(articleModel.getImageUrl())) {
				articleModel.setImageUrl(post+articleModel.getImageUrl());
			}
		}
		
		int count = articleMapper.queryCountArticleList(map);
		
		page.setList(list);
		page.setCount(count);
		page.setPageNo(param.getPageNo());
		page.setPageSize(param.getPageSize());
		
		return page;
	}
	
	@Override
	public Page<ArticleModel> queryArticleByCategoryId(Param param) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("categoryId", param.getCategoryId());
		
		map.put("start", (param.getPageNo()-1)*param.getPageSize());
		map.put("end", param.getPageSize());
		
		Page<ArticleModel> page =new Page<ArticleModel>();
		
		List<ArticleModel> list = articleMapper.queryArticleByCategoryId(map);
		
		int count = articleMapper.queryCountArticleByCategoryId(map);
		
		page.setList(list);
		page.setCount(count);
		page.setPageNo(param.getPageNo());
		page.setPageSize(param.getPageSize());
		
		return page;
	}

	@Override
	public int updateArticle(ArticleModel articleModel) throws Exception {
		
		articleModel.setModified(1);
		articleModel.setUpdateTime(DateUtils.formatTimestampDate(new Date()));
		
		int i = articleMapper.updateArticle(articleModel);
		
		if(i<=0) {
			throw new MyControlException("系统错误！修改失败！");
		}
		
		return i;
	}

	@Override
	public Integer deleteArticle(List<Integer> ids) throws Exception {
		
		return articleMapper.deleteArticle(ids);
	}

	@Override
	public ArticleModel queryArticleById(Long id) throws Exception {
		
		ArticleModel queryArticleById = articleMapper.queryArticleById(id);
		
		if(StringUtils.isNoneEmpty(queryArticleById.getImageUrl())) {
			queryArticleById.setImageUrl(post+queryArticleById.getImageUrl());
		}
		
		return queryArticleById;
	}

	@Override
	public List<ArticleModel> queryArticleUpDown(Long id) throws Exception {
		
		List<ArticleModel> list = new ArrayList<ArticleModel>();
		
		List<ArticleModel> queryArticleUpDown = articleMapper.queryArticleUpDown(id);
		
		
		for (ArticleModel articleModel : queryArticleUpDown) {
			
			Integer parentId = articleCategoryMapper.queryCategoryParentById(articleModel.getCategoryId());
			articleModel.setCategoryParent(parentId);
			
			if(StringUtils.isNoneEmpty(articleModel.getImageUrl())) {
				articleModel.setImageUrl(post+articleModel.getImageUrl());
			}
		}
		
		if(queryArticleUpDown != null ) {
		
			for(int i = 0 ; i<queryArticleUpDown.size() ; i++) {
	
				if(id.equals(queryArticleUpDown.get(i).getId()) ) {
					
					list.add(queryArticleUpDown.get(i));
					
					if(i != 0) {
						list.add(queryArticleUpDown.get(i-1));
					}
					if(i != queryArticleUpDown.size()-1) {
						list.add(queryArticleUpDown.get(i+1));
					}
					
				}
				
			}
		}
		
		return list;
	}
	
	
	@Override
	public List<ArticleModel> articleUpDown(Long id) throws Exception {
		
		List<ArticleModel> list = new ArrayList<ArticleModel>();
		
		List<ArticleModel> queryArticleUpDown = articleMapper.queryArticleUpDown(id);
		for (ArticleModel articleModel : queryArticleUpDown) {
			Integer parentId = articleCategoryMapper.queryCategoryParentById(articleModel.getCategoryId());
			articleModel.setCategoryParent(parentId);
			
			if(StringUtils.isNoneEmpty(articleModel.getImageUrl())) {
				articleModel.setImageUrl(post+articleModel.getImageUrl());
			}
		}
		
		if(queryArticleUpDown != null ) {
			
			
			for(int i = 0 ; i<queryArticleUpDown.size() ; i++) {
				
				if(id.equals(queryArticleUpDown.get(i).getId()) ) {
					
					if(i != 0) {
						list.add(queryArticleUpDown.get(i-1));
					}else {
						list.add(new ArticleModel());
					}
					
					list.add(queryArticleUpDown.get(i));
					
					
					if(i != queryArticleUpDown.size()-1) {
						list.add(queryArticleUpDown.get(i+1));
					}else{
						list.add(new ArticleModel());
					}
					
				}
				
			}
		}
		
		return list;
	}

	@Override
	public List<ArticleModel> queryHotArticle() throws Exception {
		
		List<ArticleModel> hotArticle = articleMapper.queryHotArticle();
		
		for (ArticleModel articleModel : hotArticle) {
			Integer parentId = articleCategoryMapper.queryCategoryParentById(articleModel.getCategoryId());
			articleModel.setCategoryParent(parentId);
			if(StringUtils.isNoneEmpty(articleModel.getImageUrl())) {
				articleModel.setImageUrl(post+articleModel.getImageUrl());
			}
		}
		
		return hotArticle;
	}

}
