package com.jingshi.qf.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jingshi.qf.common.Page;
import com.jingshi.qf.mapper.ResumeMapper;
import com.jingshi.qf.pojo.ResumeModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.ResumeService;

@Service
public class ResumeServiceImpl implements ResumeService {
	
	@Value("${wangzhan.post}")
	private String post;
	
	@Autowired
	private ResumeMapper resumeMapper;

	@Override
	public Page<ResumeModel> queryResumeList(Param param) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", (param.getPageNo()-1)*param.getPageSize());
		map.put("end", param.getPageSize());
		
		Page<ResumeModel> page =new Page<ResumeModel>();
		
		List<ResumeModel> list = resumeMapper.queryResumeList(map);
		for (ResumeModel resumeModel : list) {
			if(StringUtils.isNoneEmpty(resumeModel.getBigImageUrl())) {
				resumeModel.setBigImageUrl(post+resumeModel.getBigImageUrl());
			}
			if(StringUtils.isNoneEmpty(resumeModel.getSmImageUrl())) {
				resumeModel.setSmImageUrl(post+resumeModel.getSmImageUrl());
			}
		}
		
		int count = resumeMapper.queryCountResumeList(map);
		
		page.setList(list);
		page.setCount(count);
		page.setPageNo(param.getPageNo());
		page.setPageSize(param.getPageSize());
		
		return page;
	}

	@Override
	public ResumeModel queryResumeById(Long id) throws Exception {
		
		ResumeModel resume = resumeMapper.queryResumeById(id);
		if(StringUtils.isNoneEmpty(resume.getImageUrl())) {
			resume.setImageUrl(post+resume.getImageUrl());
		}
		
		return resume;
	}

}
