package com.jingshi.qf.service;

import com.jingshi.qf.common.Page;
import com.jingshi.qf.pojo.FileModel;
import com.jingshi.qf.pojo.requestParam.Param;

public interface VideoService {
	
	Page<FileModel> queryVideoList(Param param)throws Exception;
	
	FileModel queryVideoById(Long id)throws Exception;

}
