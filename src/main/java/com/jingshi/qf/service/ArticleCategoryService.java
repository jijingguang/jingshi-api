package com.jingshi.qf.service;

import java.util.List;

import com.jingshi.qf.pojo.ArticleCategoryModel;

public interface ArticleCategoryService {
	
	int addArticleCategory(ArticleCategoryModel articleCategory)throws Exception;
	
	List<ArticleCategoryModel> queryArticleCategoryList()throws Exception;
	
	int updateArticleCategory(ArticleCategoryModel articleCategory)throws Exception;
	
	Integer deleteArticleCategory(List<Integer> ids)throws Exception;

}
