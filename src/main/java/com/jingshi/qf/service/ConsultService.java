package com.jingshi.qf.service;

import com.jingshi.qf.pojo.ConsultModel;
import com.jingshi.qf.pojo.RemoveConsultModel;

public interface ConsultService {
	
	int addConsult(ConsultModel consultModel)throws Exception;
	
	int addRemoveConsult(RemoveConsultModel removeConsultModel)throws Exception;

}
