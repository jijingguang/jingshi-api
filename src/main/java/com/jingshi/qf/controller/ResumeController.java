package com.jingshi.qf.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.Page;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.ResumeModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.ResumeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags={"律师团队"} )
@RestController
@RequestMapping("/manager/resume")
public class ResumeController {
	
	private Logger log = LoggerFactory.getLogger(ResumeController.class);
	
	@Autowired
	private ResumeService resumeService;
	
	
	/**
	 * 查询人物简历列表
	 */
	@ApiOperation(value = "查询人物简历列表" ,  notes="查询人物简历列表")
	@RequestMapping(value="queryResumeList" , method=RequestMethod.POST)
	public ResultBean<Page<ResumeModel>> queryResumeList(@RequestBody Param param) throws Exception{
		
		log.info("查询菜单列表！");
		Page<ResumeModel> page = resumeService.queryResumeList(param);
		
		return new ResultBean<Page<ResumeModel>>(true,0,"查询成功",page);
	}
	
	/**
	 * 查询人简历
	 */
	@ApiOperation(value = "查询人履历" ,  notes="查询人履历")
	@GetMapping("/queryResumeById")
	public ResultBean<ResumeModel> queryResumeById(Long id) throws Exception{
		
		if(id == null || id == 0) {
			throw new MyControlException("id不能为空！");
		}
		
		ResumeModel resume = resumeService.queryResumeById(id);
		
		return new ResultBean<ResumeModel>(true,0,"查询成功",resume);
	}

}
