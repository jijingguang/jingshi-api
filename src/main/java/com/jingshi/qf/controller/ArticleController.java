package com.jingshi.qf.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.Page;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.ArticleModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.ArticleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(tags={"文章"} )
@RestController
@RequestMapping("/manager/article")
public class ArticleController {
	
	private static Logger log = LoggerFactory.getLogger(ArticleController.class);
	
	@Autowired
	private ArticleService articleService;
	
	/**
	 * 查询文章列表
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "查询文章列表" ,  notes="查询文章列表")
	@PostMapping("/queryArticleList")
	public ResultBean<Page<ArticleModel>> queryArticleList(@RequestBody Param param) throws Exception{
		
		log.info("查询文章列表参数"+param.toString());
		
		
		Page<ArticleModel> page = articleService.queryArticleList(param);
		
		return new ResultBean<Page<ArticleModel>>(true,0,"查询成功",page);
	}
	
	/**
	 * 查询含有二级分类的文章列表
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/queryArticleByCategoryId")
	public ResultBean<Page<ArticleModel>> queryArticleByCategoryId(@RequestBody Param param) throws Exception{
		
		log.info("查询含有二级分类的文章参数"+param.toString());
		
		if(param.getCategoryId() == null ) {
			throw new MyControlException("请输入标文章分类！");
		}
		
		Page<ArticleModel> page = articleService.queryArticleByCategoryId(param);
		
		return new ResultBean<Page<ArticleModel>>(true,0,"查询成功",page);
	}
	
	@ApiOperation(value = "查询文章详情" ,  notes="查询文章详情")
	@GetMapping("/queryArticleById")
	public ResultBean<ArticleModel> queryArticleById(Long id)throws Exception {
		
		log.info("查询文章详情参数"+id);
		if(id == null || id == 0) {
			throw new MyControlException("文章id不能为空！");
		}
		
		ArticleModel articleModel = articleService.queryArticleById(id);
		
		return new ResultBean<ArticleModel>(true,0,"查询成功",articleModel);
		
	}
	
	
	@GetMapping("/queryArticleUpDown")
	public ResultBean<List<ArticleModel>> queryArticleUpDown(Long id)throws Exception {
		
		log.info("查询上篇下篇参数"+id);
		if(id == null || id == 0) {
			throw new MyControlException("文章id不能为空！");
		}
		
		List<ArticleModel> list = articleService.queryArticleUpDown(id);
		
		
		return new ResultBean<List<ArticleModel>>(true,0,"查询成功",list);
		
	}
	
	
	@ApiOperation(value = "上下篇改" ,  notes="上下篇改")
	@GetMapping("/articleUpDown")
	public ResultBean<List<ArticleModel>> articleUpDown(Long id)throws Exception {
		
		log.info("查询上篇下篇参数"+id);
		if(id == null || id == 0) {
			throw new MyControlException("文章id不能为空！");
		}
		
		List<ArticleModel> list = articleService.articleUpDown(id);
		
		
		return new ResultBean<List<ArticleModel>>(true,0,"查询成功",list);
		
	}
	
	@ApiOperation(value = "热门文章" ,  notes="热门文章")
	@GetMapping("/queryHotArticle")
	public ResultBean<List<ArticleModel>> queryHotArticle()throws Exception {
		
		log.info("热门文章");
		
		List<ArticleModel> list = articleService.queryHotArticle();
		
		
		return new ResultBean<List<ArticleModel>>(true,0,"查询成功",list);
		
	}
	

}
