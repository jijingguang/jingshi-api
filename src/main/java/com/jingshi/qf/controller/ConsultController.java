package com.jingshi.qf.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.ConsultModel;
import com.jingshi.qf.pojo.RemoveConsultModel;
import com.jingshi.qf.service.ConsultService;

import io.swagger.annotations.Api;


@Api(tags={"咨询"} )
@RestController
@RequestMapping("/manager/consult")
public class ConsultController {
	
	private static Logger log = LoggerFactory.getLogger(ConsultController.class);
	
	@Autowired
	private ConsultService consultService;
	
	@PostMapping("/addConsult")
	public ResultBean<?> addConsult(@RequestBody ConsultModel consultModel)throws Exception{
		
		log.info("开始添加咨询！");
		if(StringUtils.isEmpty(consultModel.getName())) {
			throw new MyControlException("请输入姓名！");
		}
		if(StringUtils.isEmpty(consultModel.getMobile())) {
			throw new MyControlException("请输入手机号！");
		}
//		if(StringUtils.isEmpty(consultModel.getAddress())) {
//			throw new MyControlException("请输入住址！");
//		}
//		if(StringUtils.isEmpty(consultModel.getConsultDescribe())) {
//			throw new MyControlException("请填写案情描述！");
//		}
//		
		consultService.addConsult(consultModel);
		
		return new ResultBean<>(true,0,"添加成功！");
	}
	/**
	 * 添加拆迁咨询
	 */
	@PostMapping("/addRemoveConsult")
	public ResultBean<?> addRemoveConsult(@RequestBody RemoveConsultModel removeConsultModel)throws Exception{
		
		log.info("开始添加咨询！");
		if(StringUtils.isEmpty(removeConsultModel.getName())) {
			throw new MyControlException("请输入姓名！");
		}
		if(StringUtils.isEmpty(removeConsultModel.getMobile())) {
			throw new MyControlException("请输入手机号！");
		}
		
		if(removeConsultModel.getRemoveScope() == null) {
			throw new MyControlException("请选择动迁范围！");
		}
		if(removeConsultModel.getRemoveSchedule() == null) {
			throw new MyControlException("请选择动迁进度！");
		}
		if(removeConsultModel.getPropertyFormalities() == null 	) {
			throw new MyControlException("请选择房产手续！");
		}
		if(removeConsultModel.getUrl() == null 	) {
			throw new MyControlException("请添加来源路径！");
		}
		
		consultService.addRemoveConsult(removeConsultModel);
		
		return new ResultBean<>(true,0,"添加成功！");
	}

}
