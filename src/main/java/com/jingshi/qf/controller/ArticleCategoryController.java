package com.jingshi.qf.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.ArticleCategoryModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.ArticleCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags={"文章分类"} )
@RestController
@RequestMapping("/manager/category")
public class ArticleCategoryController {
	
	private static Logger log = LoggerFactory.getLogger(ArticleCategoryController.class);
	
	@Autowired
	private ArticleCategoryService articleCategoryService;
	
	
	
	
	/**
	 * 查询分类列表
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "查询标签列表" ,  notes="查询标签列表")
	@GetMapping("/queryArticleCategoryList")
	public ResultBean<List<ArticleCategoryModel>> queryArticleCategoryList() throws Exception{
		
		
		log.info("查询标签列表！");
		List<ArticleCategoryModel> list =  articleCategoryService.queryArticleCategoryList();
		
		return new ResultBean<>(true,0,"查询成功",list);
	}

	
	
	
	
	
	/**
	 * 添加分类
	 * @param articleCategoryModel
	 * @return
	 * @throws Exception
	 */
//	@PostMapping("/addArticCategory")
//	public ResultBean<?> addArticleCategory(@RequestBody ArticleCategoryModel articleCategoryModel)throws Exception{
//		
//		log.info("开始添加分类！");
//		if(StringUtils.isEmpty(articleCategoryModel.getCategoryName())) {
//			throw new MyControlException("请输入分类名！");
//		}
//		if(articleCategoryModel.getParentId() == null) {
//			throw new MyControlException("请添加上级分类！");
//		}
//		
//		articleCategoryService.addArticleCategory(articleCategoryModel);
//		
//		return new ResultBean<>(true,0,"添加成功！");
//	}
	
	
	/**
	 * 修改分类
	 * @param articleCategoryModel
	 * @return
	 * @throws Exception
	 */
//	@PostMapping("/updateArticleCategory")
//	public ResultBean<?> updateArticleCategory(@RequestBody ArticleCategoryModel articleCategoryModel)throws Exception{
//		
//		log.info("修改分类！");
//		if(articleCategoryModel.getId() == null || articleCategoryModel.getId() ==0) {
//			throw new MyControlException("请选择要修改的分类！");
//		}
//		articleCategoryService.updateArticleCategory(articleCategoryModel);
//		
//		return new ResultBean<>(true,0,"修改成功！");
//	}
	
	/**
	 * 批量删除分类
	 * @param parm
	 * @return
	 * @throws Exception
	 */
//	@PostMapping("/deleteArticleCategory")
//	public ResultBean<?> deleteArticleCategory(@RequestBody Param parm)throws Exception{
//		
//		log.info("批量删除标签！");
//		
//		if(parm.getIds()== null || parm.getIds() .size() <=0) {
//			throw new MyControlException("请选择要删除的分类！");
//		}
//		
//		 Integer deleteMenu = articleCategoryService.deleteArticleCategory(parm.getIds());
//		
//		return new ResultBean<>(true,0,"成功删除"+deleteMenu+"条！");
//	}

	

}
