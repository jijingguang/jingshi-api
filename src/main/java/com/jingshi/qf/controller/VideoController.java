package com.jingshi.qf.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jingshi.qf.common.MyControlException;
import com.jingshi.qf.common.Page;
import com.jingshi.qf.common.ResultBean;
import com.jingshi.qf.pojo.FileModel;
import com.jingshi.qf.pojo.requestParam.Param;
import com.jingshi.qf.service.VideoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags={"律师视频"} )
@RestController
@RequestMapping("/manager/video")
public class VideoController {
	
	private Logger log = LoggerFactory.getLogger(VideoController.class);
	
	@Autowired
	private VideoService videoService;
	
	
	/**
	 * 查询视频列表
	 */
	@ApiOperation(value = "查询视频列表" ,  notes="查询视频列表")
	@RequestMapping(value="queryVideoList" , method=RequestMethod.POST)
	public ResultBean<Page<FileModel>> queryVideoList(@RequestBody Param param) throws Exception{
		
		log.info("查询视频列表！");
		Page<FileModel> page = videoService.queryVideoList(param);
		
		return new ResultBean<Page<FileModel>>(true,0,"查询成功",page);
	}
	
	/**
	 * 根据id查询视频
	 */
	@ApiOperation(value = "根据id查询视频" ,  notes="根据id查询视频")
	@RequestMapping(value="queryVideoById" , method=RequestMethod.GET)
	public ResultBean<FileModel> queryVideoById(Long id) throws Exception{
		
		log.info("根据id查询视频！");
		
		if(id == null || id == 0) {
			throw new MyControlException("id不能为空！");
		}
		FileModel fileModel = videoService.queryVideoById(id);
		
		return new ResultBean<FileModel>(fileModel);
	}
	

}
